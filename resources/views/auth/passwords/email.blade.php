@extends('layouts.basic')

<!-- Main Content -->
@section('content')
    <div class="ui container" style="max-width: 300px !important;">
        <h3 class="dividing header">Reset Password</h3>
        <div class="ui segment">
            @if (session('status'))
                <div class="ui message">
                    {{ session('status') }}
                </div>
            @endif

            <form class="ui form {{ $errors->count() ? ' error' : '' }} " method="POST"  action="{{ url('/password/email') }}">
                {{ csrf_field() }}

                @if ($errors->count())
                    <div class="ui error message">
                        <div class="header">Errors</div>
                        @foreach($errors->all() as $error)
                            <b>{{ $error }}</b><br>
                        @endforeach
                    </div>
                @endif

                <div class="field {{ $errors->has('email') ? ' error' : '' }}">
                    <label>E-mail</label>
                    <input id="email" name="email" type="email" value="{{ old('email') }}">
                </div>

                <button class="ui button" type="submit">Send Password Reset Link</button>
            </form>
        </div>
    </div>

<!--<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->
@endsection

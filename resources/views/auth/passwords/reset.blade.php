@extends('layouts.basic')

@section('content')

    <div class="ui container" style="max-width: 300px !important;">
        <h3 class="dividing header">Reset Password</h3>
        <div class="ui segment">
            <form class="ui form {{ $errors->count() ? ' error' : '' }} " method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                @if ($errors->count())
                    <div class="ui error message">
                        <div class="header">Errors</div>
                        @foreach($errors->all() as $error)
                            <b>{{ $error }}</b><br>
                        @endforeach
                    </div>
                @endif

                <div class="field {{ $errors->has('email') ? ' error' : '' }}">
                    <label>E-mail</label>
                    <input id="email" name="email" type="email" value="{{ $email or old('email') }}">
                </div>
                <div class="field {{ $errors->has('password') ? ' error' : '' }}">
                    <label>Password</label>
                    <input id="password" type="password" name="password">
                </div>
                <div class="field {{ $errors->has('password_confirmation') ? ' error' : '' }}">
                    <label>Confirm Password</label>
                    <input id="password-confirm" type="password" name="password_confirmation">
                </div>

                <button class="ui button" type="submit">Reset Password</button>

            </form>
        </div>
    </div>
@endsection

@extends('layouts.basic')

@section('content')
    <div class="ui container" style="max-width: 300px !important;">
        <h3 class="dividing header">Please login</h3>
        <div class="ui segment">
            <form class="ui form {{ $errors->count() ? ' error' : '' }} " method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                @if ($errors->count())
                    <div class="ui error message">
                        <div class="header">Errors</div>
                        @foreach($errors->all() as $error)
                            <b>{{ $error }}</b><br>
                        @endforeach
                    </div>
                @endif

                <div class="field {{ $errors->has('email') ? ' error' : '' }}">
                    <label>E-mail</label>
                    <input id="email" name="email" type="email" value="{{ old('email') }}">
                </div>
                <div class="field {{ $errors->has('password') ? ' error' : '' }}">
                    <label>Password</label>
                    <input id="password" type="password" name="password">
                </div>
                <div class="field">
                    <div class="ui checkbox">
                        <input type="checkbox" name="remember">
                        <label>Remember me</label>
                    </div>
                </div>
                <button class="ui button" type="submit">Login</button>
                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your
                    Password?</a>
            </form>
        </div>
    </div>
    <!--
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form">
                            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}">

                                    @if ($errors->has('email'))
        <span class="help-block">
        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
        <span class="help-block">
        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> Login
                </button>

                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your
                                        Password?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    -->
@endsection

@extends('layouts.app')
@section('title', 'Statuses')

@push('scripts')
<script src="/js/statuses.js"></script>
@endpush

@section('content')

    <div class="ui two columns grid">
        <div class="column">
            <h1>Statuses</h1>
        </div>
        <div class="right aligned column">
            <div class="ui blue labeled icon button" v-on:click="showCreateModal()">New <i class="plus icon"></i>
            </div>
        </div>
    </div>

    <div class="ui segment">
        <table id="crudTable" class="display" cellspacing="0" width="100%">
            <thead>
                <th>ID</th>
                <th>Status</th>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div id="crudModal" class="ui small modal">
        <i class="close icon"></i>
        <div class="header">
            <span v-html="crudActionType"></span> Status
        </div>
        <div class="content">
            <div class="ui error message" v-if="crudError" v-html="crudError"></div>
            <div id="crudForm" class="ui form">
                <div class="field">
                    <label>Status name</label>
                    <input type="text" name="name" placeholder="Status name" v-model="fields.name" v-on:keypress.enter="save()">
                </div>
            </div>
        </div>
       @include('partials.crud-modal-actions')
    </div>


@endsection
@extends('layouts.app')
@section('title', 'Schedules')

@push('scripts')
<script src="/js/schedules.js"></script>
@endpush

@section('content')

    <div class="ui two columns grid">
        <div class="column">
            <h1>Schedules</h1>
        </div>
        <div class="right aligned column">
            <div class="ui blue labeled icon button" v-on:click="showCreateModal()">New <i class="plus icon"></i>
            </div>
        </div>
    </div>

    <div class="ui segment">
        <table id="crudTable" class="display" cellspacing="0" width="100%">
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>File</th>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div id="crudModal" class="ui small modal">
        <i class="close icon"></i>
        <div class="header">
            <span v-html="crudActionType"></span> Schedule
        </div>
        <div class="content">
            <div class="ui error message" v-if="crudError" v-html="crudError"></div>
            <div id="crudForm" class="ui form">
                <div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Name" v-model="fields.name" v-on:keypress.enter="save()">
                </div>
                <div class="field">
                    <label>Schedule file</label>
                    <input type="file" id="file" name="file" placeholder="Select file" v-model="fields.file" v-on:keypress.enter="save()">
                </div>
            </div>
        </div>
        <div class="actions">
            <div v-if="crudActionType != 'Create'" class="ui red deny button" style="float: left" v-on:click="showCrudDelete()">
                Delete
            </div>
            <div class="ui black deny button">
                Cancel
            </div>
            <div class="ui green right labeled icon button" v-on:click="saveAndUploadSchedule()">
                Save
                <i class="checkmark icon"></i>
            </div>
        </div>
    </div>


@endsection
@extends('layouts.app')
@section('title', 'Reports')

@push('scripts')
<script src="/js/reports.js"></script>
@endpush

@section('content')

    <div class="ui two columns grid">
        <div class="column">
            <h1>Reports</h1>
        </div>

    </div>

    <div class="ui segment">
        <h3>By Specialty</h3>
        <table class="display compact">
            <thead>
            <th>Specialty</th>
            <th>Total</th>
            </thead>
            <tbody>
            @foreach($reports['bySpecialty'] as $line)
                <tr>
                    <td>{{ $line->specialty }}</td>
                    <td>{{ $line->total }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="ui segment">
        <h3>By Type</h3>
        <table class="display compact">
            <thead>
            <th>Type</th>
            <th>Total</th>
            </thead>
            <tbody>
            @foreach($reports['byType'] as $line)
                <tr>
                    <td>{{ $line->provider_type }}</td>
                    <td>{{ $line->total }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="ui segment">
        <h3>By State</h3>
        <table class="display compact">
            <thead>
            <th>State name</th>
            <th>State abbreviation</th>
            <th>Total</th>
            </thead>
            <tbody>
            @foreach($reports['byState'] as $line)
                <tr>
                    <td>{{ isset(\App\States::$states[$line->state]) ? \App\States::$states[$line->state] : '' }}</td>
                    <td>{{ $line->state }}</td>
                    <td>{{ $line->total }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="ui segment">
        <h3>By City</h3>
        <table class="display compact">
            <thead>
            <th>City</th>
            <th>State</th>
            <th>Total</th>
            </thead>
            <tbody>
            @foreach($reports['byCity'] as $line)
                <tr>
                    <td>{{ $line->city }}</td>
                    <td>{{ $line->state }}</td>
                    <td>{{ $line->total }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

<br>
<br>
@endsection
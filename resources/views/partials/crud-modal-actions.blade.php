<div class="actions">
    <div v-if="crudActionType != 'Create'" class="ui red deny button" style="float: left" v-on:click="showCrudDelete()">
        Delete
    </div>
    <div class="ui black deny button">
        Cancel
    </div>
    <div class="ui green right labeled icon button" v-on:click="save()">
        Save
        <i class="checkmark icon"></i>
    </div>
</div>
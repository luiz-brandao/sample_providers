@extends('layouts.app')
@section('title', 'Providers')

@push('scripts')
<script src="/js/providers.js"></script>
@endpush

@section('content')

    <div class="ui two columns grid">
        <div class="column">
            <h1>Providers</h1>
        </div>
        <div class="right aligned column">
            <label for="import-upload">
                <div id="import-upload-btn" class="ui grey labeled icon button">
                    Import<i class="upload icon"></i>
                </div>
                <input type="file" id="import-upload" style="display:none" v-on:change="importFile()">
            </label>


            <div class="ui dropdown purple labeled icon floating button">
                Export  <i class="download icon"></i>
                <div class="menu">
                    <a href="/provider/export/full" class="item"><i class="file text icon"></i>Default format</a>
                    <a href="/provider/export/dpi" class="item"><i class="file text icon"></i> DPI format</a>
                </div>
            </div>

            <div class="ui blue labeled icon button" v-on:click="showCreateModal()">New <i class="plus icon"></i></div>
        </div>
    </div>

    <div class="ui segment">
        <table id="crudTable" class="display compact nowrap" cellspacing="0" width="100%">
            <thead>
            <th>Id</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Practice name</th>
            <th>City</th>
            <th>State</th>
            <th>Zipcode</th>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div id="crudModal" class="ui modal">
        <i class="close icon"></i>
        <div class="header">
            <span v-html="crudActionType"></span> Provider
        </div>
        <div class="content">
            <div class="ui error message" v-if="crudError" v-html="crudError"></div>
            <div id="crudForm" class="ui small form">
                <div class="ui grid">
                    <div class="eleven wide column">
                        <h4 class="ui dividing header">Provider </h4>
                        <div class="field">
                            <label>Name</label>
                            <div class="two fields">
                                <div class="field">
                                    <input type="text" name="first_name" placeholder="First Name"
                                           v-model="fields.first_name">
                                </div>
                                <div class="field">
                                    <input type="text" name="last_name" placeholder="Last Name"
                                           v-model="fields.last_name">
                                </div>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="twelve wide field">
                                <label>Practice name</label>
                                <input type="text" name="practice_name" v-model="fields.practice_name"
                                       placeholder="Practice name">
                            </div>
                            <div class="four wide field">
                                <label>Tax ID</label>
                                <input type="text" name="tax_id" v-model="fields.tax_id" placeholder="Tax ID">
                            </div>
                        </div>

                        <div class="field">
                            <label>Specialties</label>
                            <div id="specialties-select" class="ui dropdown selection multiple" tabindex="0">
                                <select id="specialties"
                                        name="specialties"
                                        multiple="" v-model="fields.specialties">
                                    <option v-for="specialty in options.specialties" v-bind:value="specialty.id"
                                            v-html="specialty.name"></option>
                                </select>
                                <i class="dropdown icon"></i>
                                <div class="text default"></div>
                                <div class="menu" tabindex="-1">
                                    <div v-for="specialty in options.specialties" class="item"
                                         v-bind:data-value="specialty.id" v-html="specialty.name">Endodontist
                                    </div>
                                </div>
                            </div>

                            <!--<select  class="ui dropdown" id="specialties"  v-model="fields.specialties" name="specialty_id"  multiple>
                                <option v-for="specialty in options.specialties" v-bind:value="specialty.id"
                                        v-html="specialty.name"></option>
                            </select>-->
                        </div>


                        <div class="three fields">
                            <div class="field">
                                <label>Provider type</label>
                                <select v-model="fields.provider_type_id" name="provider_type_id">
                                    <option v-for="type in options.provider_types" v-bind:value="type.id"
                                            v-html="type.name"></option>
                                </select>
                            </div>
                            <div class="field">
                                <label>Fee Schedule</label>
                                <select v-model="fields.schedule_id" name="schedule_id">
                                    <option value=""></option>

                                    <option v-for="schedule in options.schedules" v-bind:value="schedule.id"
                                            v-html="schedule.name"></option>
                                </select>
                            </div>
                            <div class="field">
                                <label>Status</label>
                                <select v-model="fields.status_id" name="status_id">
                                    <option value=""></option>
                                    <option v-for="status in options.statuses" v-bind:value="status.id"
                                            v-html="status.name"></option>
                                </select>
                            </div>
                        </div>

                        <h4 class="ui dividing header">Address</h4>
                        <div class="field">
                            <label>Address</label>
                            <div class="fields">
                                <div class="twelve wide field">
                                    <input type="text" name="address1" placeholder="Street Address"
                                           v-model="fields.address1">
                                </div>
                                <div class="four wide field">
                                    <input type="text" name="address2" placeholder="Apt #" v-model="fields.address2">
                                </div>
                            </div>
                        </div>
                        <div class="three fields">
                            <div class="field">
                                <label>City</label>
                                <input type="text" name="city" placeholder="City" v-model="fields.city">
                            </div>
                            <div class="field">
                                <label>State</label>
                                <select name="state" v-model="fields.state">
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                </select>
                            </div>
                            <div class="field">
                                <label>Zipcode</label>
                                <input type="text" name="zipcode" placeholder="Zipcode" v-model="fields.zipcode">
                            </div>
                        </div>

                        <h4 class="ui dividing header">Contact Information</h4>
                        <div class="three fields">
                            <div class="field">
                                <label>Phone</label>
                                <input type="text" name="phone1" v-model="fields.phone1">
                            </div>
                            <div class="field">
                                <label>Phone 2</label>
                                <input type="text" name="phone2" v-model="fields.phone2">
                            </div>
                            <div class="field">
                                <label>Fax</label>
                                <input type="text" name="fax" v-model="fields.fax">
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field">
                                <label>Email</label>
                                <input type="text" name="email" v-model="fields.email">
                            </div>
                            <div class="field">
                                <label>Website</label>
                                <input type="text" name="website" v-model="fields.website">
                            </div>
                        </div>


                    </div>
                    <div class="five wide column">

                        <h4 class="ui dividing header">Location Officer Manager</h4>

                        <div class="field">
                            <label>Name</label>
                            <input type="text" name="location_office_manager_name"
                                   v-model="fields.location_office_manager_name">
                        </div>
                        <div class="field">
                            <label>Phone</label>
                            <input type="text" name="location_office_manager_phone"
                                   v-model="fields.location_office_manager_phone">
                        </div>
                        <div class="field">
                            <label>Email</label>
                            <input type="text" name="location_office_manager_email"
                                   v-model="fields.location_office_manager_email">
                        </div>
                        <h4 class="ui dividing header">Active Date</h4>
                        <div>
                            <input type="date" name="active_date"
                                   v-model="fields.active_date">
                        </div>
                        <h4 class="ui dividing header">Agreement Files</h4>
                        <div class="files-list" style="margin-bottom: 10px">
                            <table style="width: 100%">
                                <tr v-for="agreement in fields.agreements" style="margin-bottom: 5px">
                                    <td>
                                        <a v-bind:href="agreement.filepath">
                                            <i class="file pdf outline icon"></i> <span
                                                    v-html="agreement.filename"></span>
                                        </a>
                                    </td>
                                    <td style="text-align: right; padding-bottom: 5px">
                                        <div class="ui mini icon button" v-on:click="deleteAgreement(agreement.id)"><i
                                                    class="delete icon"></i></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="files-list" style="margin-bottom: 10px">
                            <div v-for="n in totalNewFiles" class="field">
                                <input type="file" class="agreement-file" name="agreement_files[]">
                            </div>
                        </div>

                        <div class="ui tiny button" v-on:click="addFile"><i class="add icon"></i> Add file
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div class="actions">
            <div v-if="crudActionType != 'Create'" class="ui red deny button" style="float: left"
                 v-on:click="showCrudDelete()">
                Delete
            </div>
            <div class="ui black deny button">
                Cancel
            </div>
            <div class="ui green right labeled icon button" v-on:click="saveAndUploadProvider()">
                Save
                <i class="checkmark icon"></i>
            </div>
        </div>
    </div>


@endsection
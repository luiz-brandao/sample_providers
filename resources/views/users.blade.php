@extends('layouts.app')
@section('title', 'Users')

@push('scripts')
<script src="/js/users.js"></script>
@endpush

@section('content')

    <div class="ui two columns grid">
        <div class="column">
            <h1>Users</h1>
        </div>
        <div class="right aligned column">
            <div class="ui blue labeled icon button" v-on:click="showCreateModal()">New <i class="plus icon"></i>
            </div>
        </div>
    </div>

    <div class="ui segment">
        <table id="crudTable" class="display" cellspacing="0" width="100%">
            <thead>
                <th>Name</th>
                <th>Email</th>
                <th>Type</th>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div id="crudModal" class="ui small modal">
        <i class="close icon"></i>
        <div class="header">
            <span v-html="crudActionType"></span> User
        </div>
        <div class="content">
            <div class="ui error message" v-if="crudError" v-html="crudError"></div>
            <div id="crudForm" class="ui form">
                <div class="field">
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Name" v-model="fields.name">
                </div>
                <div class="field">
                    <label>Email</label>
                    <input type="text" name="email" placeholder="Email" v-model="fields.email">
                </div>
                <div class="field">
                    <label>Password</label>
                    <input type="text" name="password" placeholder="Password" v-model="fields.password">
                </div>
                <div class="field">
                    <div class="ui checkbox">
                        <input type="checkbox"  name="is_admin"  v-model="fields.is_admin">
                        <label>Admin</label>
                    </div>
                </div>
            </div>
        </div>
       @include('partials.crud-modal-actions')
    </div>


@endsection
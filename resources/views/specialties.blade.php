@extends('layouts.app')
@section('title', 'Specialties')

@push('scripts')
<script src="/js/specialties.js"></script>
@endpush

@section('content')

    <div class="ui two columns grid">
        <div class="column">
            <h1>Specialties</h1>
        </div>
        <div class="right aligned column">
            <div class="ui blue labeled icon button" v-on:click="showCreateModal()">New <i class="plus icon"></i>
            </div>
        </div>
    </div>

    <div class="ui segment">
        <table id="crudTable" class="display" cellspacing="0" width="100%">
            <thead>
                <th>ID</th>
                <th>Specialty</th>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div id="crudModal" class="ui small modal">
        <i class="close icon"></i>
        <div class="header">
            <span v-html="crudActionType"></span> Specialty
        </div>
        <div class="content">
            <div class="ui error message" v-if="crudError" v-html="crudError"></div>
            <div id="crudForm" class="ui form">
                <div class="field">
                    <label>Specialty</label>
                    <input type="text" name="name" placeholder="Specialty" v-model="fields.name" v-on:keypress.enter="save()">
                </div>
            </div>
        </div>
       @include('partials.crud-modal-actions')
    </div>


@endsection
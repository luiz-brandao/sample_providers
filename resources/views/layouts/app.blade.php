<!DOCTYPE html>
<html>
<head>
    <title>App Name - @yield('title')</title>

    <link rel="stylesheet" type="text/css" href="/bower_components/datatables/media/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="/bower_components/semantic/dist/semantic.css">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    @stack('styles')

    <script type="text/javascript" src="/bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="/bower_components/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/bower_components/vue/dist/vue.js"></script>
    <script type="text/javascript" src="/bower_components/vue-resource/dist/vue-resource.js"></script>
    <script type="text/javascript" src="/bower_components/semantic/dist/semantic.js"></script>
    <script type="text/javascript" src="/js/app.js"></script>
    <script type="text/javascript" src="/js/crud.js"></script>
    @stack('scripts')

</head>
<body>

<div class="ui top inverted fixed menu">
    <div class="ui container">
        <div class="item">
            <b>AFHS Providers</b>
        </div>
        <a href="/" class="item">Providers</a>
        <a href="/provider-types" class="item">Provider Types</a>
        <a href="/specialties" class="item">Specialties</a>
        <a href="/schedules" class="item">Schedules</a>
        <a href="/statuses" class="item">Statuses</a>
        <a href="/reports" class="item">Reports</a>
        <a href="/zipcode" class="item">Zipcode Search</a>
        @can('manage-users')
            <a href="/users" class="item">Users</a>
        @endcan
        <div class="right menu">
            <a href="/logout" class="item">Logout</a>
        </div>
        {{--   <a href="/users" class="item">Users</a>
           <a href="/" class="item">Sign-in</a>--}}
    </div>

</div>

<div class="ui container">
    @yield('content')
</div>

<div id="confirmDeleteModal" class="ui basic modal">
    <div class="ui icon header"><i class="trash icon"></i> Are you sure you want to delete this resource?</div>

    <div class="actions">
        <div class="ui cancel red inverted button">
            <i class="remove icon"></i>
            No
        </div>
        <div class="ui green ok inverted button">
            <i class="checkmark icon"></i>
            Yes
        </div>

    </div>
</div>

</body>
</html>
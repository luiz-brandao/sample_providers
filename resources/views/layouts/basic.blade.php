<!DOCTYPE html>
<html>
<head>
    <title>App Name - @yield('title')</title>

    <link rel="stylesheet" type="text/css" href="/bower_components/semantic/dist/semantic.css">
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    @stack('styles')

    <script type="text/javascript" src="/bower_components/jquery/dist/jquery.js"></script>
    <script type="text/javascript" src="/bower_components/vue/dist/vue.js"></script>
    <script type="text/javascript" src="/bower_components/vue-resource/dist/vue-resource.js"></script>
    <script type="text/javascript" src="/bower_components/semantic/dist/semantic.js"></script>
    <script type="text/javascript" src="/js/app.js"></script>
    @stack('scripts')

</head>
<body>

<div class="ui top inverted fixed menu">
    <div class="ui container">
        <div class="item">
            <b>AFHS Providers</b>
        </div>
        <div class="right menu">
            <a href="/login" class="item">Login</a>
        </div>

    </div>

</div>

<div class="ui container">
    @yield('content')
</div>

</body>
</html>
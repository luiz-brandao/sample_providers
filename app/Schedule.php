<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Schedule
 *
 * @property integer $id
 * @property string $name
 * @property string $schedule_id
 * @property string $filepath
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Provider[] $providers
 * @method static \Illuminate\Database\Query\Builder|\App\Schedule whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Schedule whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Schedule whereScheduleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Schedule whereFilepath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Schedule whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Schedule whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Schedule extends Model
{
    protected $guarded = ['id'];
    
    public function providers()
    {
        return $this->hasMany('App\Provider');
    }
}

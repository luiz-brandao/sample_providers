<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Specialty
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Provider[] $providers
 * @method static \Illuminate\Database\Query\Builder|\App\Specialty whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Specialty whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Specialty whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Specialty whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Specialty extends Model
{
    protected $guarded = ['id'];
    
    public function providers()
    {
        return $this->belongsToMany('App\Provider');

    }
}

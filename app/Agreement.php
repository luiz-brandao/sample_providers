<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Agreement
 *
 * @property integer $id
 * @property string $filepath
 * @property \Carbon\Carbon $created_at
 * @property integer $provider_id
 * @property string $created_at_copy1
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Provider $provider
 * @method static \Illuminate\Database\Query\Builder|\App\Agreement whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Agreement whereFilepath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Agreement whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Agreement whereProviderId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Agreement whereCreatedAtCopy1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Agreement whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Agreement extends Model
{
    protected $guarded = ['id'];
    
    public function provider()
    {
        return $this->belongsTo('App\Provider');
    }
}

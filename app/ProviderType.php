<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ProviderType
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Provider[] $providers
 * @method static \Illuminate\Database\Query\Builder|\App\ProviderType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProviderType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProviderType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProviderType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProviderType extends Model
{
    protected $guarded = ['id'];
    
    public function providers()
    {
        return $this->hasMany('App\Provider');
    }
}

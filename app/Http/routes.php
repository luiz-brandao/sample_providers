<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('providers');
    });

    Route::get('/agreements', function () {
        return view('agreements');
    });

    Route::get('/provider-types', function () {
        return view('provider-types');
    });

    Route::get('/schedules', function () {
        return view('schedules');
    });

    Route::get('/specialties', function () {
        return view('specialties', ['specialties' => \App\Specialty::all()]);
    });

    Route::get('/statuses', function () {
        return view('statuses');
    });

    Route::get('/users', function () {
        return view('users');
    });

    Route::get('/reports', 'ProviderController@reports');
    Route::get('provider/export/{type?}', 'ProviderController@export');

    Route::get('provider/options', 'ProviderController@options');
    Route::get('provider/agreements', 'ProviderController@options');
    Route::post('provider/import', 'ProviderController@import');
    Route::delete('provider/agreement/{id}', 'ProviderController@deleteAgreement');

    Route::resource('agreement', 'AgreementController', ['except' => ['create', 'edit']]);
    Route::resource('provider', 'ProviderController', ['except' => ['create', 'edit']]);
    Route::resource('provider-type', 'ProviderTypeController', ['except' => ['create', 'edit']]);
    Route::resource('schedule', 'ScheduleController', ['except' => ['create', 'edit']]);
    Route::resource('specialty', 'SpecialtyController', ['except' => ['create', 'edit']]);
    Route::resource('status', 'StatusController', ['except' => ['create', 'edit']]);
    Route::resource('user', 'UserController', ['except' => ['create', 'edit']]);

});


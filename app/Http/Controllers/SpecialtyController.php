<?php

namespace App\Http\Controllers;

use App\Specialty;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class SpecialtyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Datatables::of(Specialty::query())
            ->setRowId('id')
            ->make(true);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $specialty = Specialty::create(Input::get());

        return response()->json(['resource' => $specialty]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $specialty = Specialty::select(['id', 'name'])->findOrFail($id);

        return response()->json(['resource' => $specialty]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $specialty = Specialty::findOrFail($id)->update(Input::get());

        return response()->json(['resource' => $specialty]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Specialty::destroy($id);

        return response()->json(['status' => true]);
    }
}

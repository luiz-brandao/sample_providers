<?php

namespace App\Http\Controllers;

use App\Agreement;
use App\Provider;
use App\ProviderType;
use App\Schedule;
use App\Specialty;
use App\Status;
use League\Csv\Writer;
use Log;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use League\Csv\Reader;
use PHPExcel_IOFactory;
use Yajra\Datatables\Facades\Datatables;

class ProviderController extends Controller
{
    protected $fileDestinationPath;

    public function __construct()
    {
        $this->fileDestinationPath = public_path('agreement_files');

        ini_set('post_max_size', '10M');
        ini_set('upload_max_filesize', '10M');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Provider::select(['id', 'first_name', 'last_name', 'practice_name', 'city', 'state', 'zipcode']);

        return Datatables::of($schedules)
            ->setRowId('id')
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Input::get();

        $specialties = explode(',', $data['specialties']);
        unset($data['specialties']);

        $provider = Provider::create($data);

        $provider->specialties()->attach($specialties);

        $this->saveAgreements($provider);

        return response()->json(['resource' => $provider]);
    }

    /**
     * @param $provider
     */
    protected function saveAgreements($provider)
    {
        $files = Input::file('agreement_files');

        if (!$files) {
            return;
        }

        foreach ($files as $file) {
            if ($file->isValid()) {
                $originalName = $file->getClientOriginalName();

                $filepath = $file->move($this->fileDestinationPath, $originalName);

                $agreement = new Agreement([
                    'filename' => $originalName,
                    'filepath' => $filepath
                ]);

                $provider->agreements()->save($agreement);
            } else {
                abort(500, 'Invalid file.');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider = Provider::findOrFail($id);
        
        $provider['agreements'] = $provider->agreements;
        $provider['specialties'] = $provider->specialties;

        return response()->json(['resource' => $provider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $provider = Provider::findOrFail($id);

        $data = Input::get();

        $specialties = explode(',', $data['specialties']);
        unset($data['specialties']);

        $provider->update($data);

        $provider->specialties()->detach();
        $provider->specialties()->attach($specialties);

        $this->saveAgreements($provider);

        return response()->json(['resource' => $provider]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAgreement($id)
    {
        Agreement::destroy($id);

        return response()->json(['status' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Provider::destroy($id);

        return response()->json(['status' => true]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function options()
    {
        $options = [
            'specialties' => Specialty::all(),
            'provider_types' => ProviderType::all(),
            'schedules' => Schedule::all(),
            'statuses' => Status::all()
        ];

        return response()->json($options);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function agreements()
    {
        $options = [
            'specialties' => Specialty::all(),
            'provider_types' => ProviderType::all(),
            'schedules' => Schedule::all(),
            'statuses' => Status::all()
        ];

        return response()->json($options);
    }

    public function reports()
    {
        // by specialty
        $sql = "SELECT s.name specialty, COUNT(*) total FROM providers p JOIN provider_specialty ps ON ps.provider_id = p.id JOIN specialties s ON ps.specialty_id = s.id GROUP BY specialty";
        $bySpecialty = DB::select($sql);

        // by type
        $sql = "SELECT pt.name provider_type, COUNT(*) total FROM providers p JOIN provider_types pt ON p.provider_type_id = pt.id GROUP BY provider_type";
        $byType = DB::select($sql);

        // by state
        $sql = "SELECT state, COUNT(*) total FROM providers GROUP BY state";
        $byState = DB::select($sql);

        // by city
        $sql = "SELECT city, state, COUNT(*) total FROM providers GROUP BY city, state";
        $byCity = DB::select($sql);

        return view('reports', ['reports' => [
            'bySpecialty' => $bySpecialty,
            'byType' => $byType,
            'byState' => $byState,
            'byCity' => $byCity
        ]]);

    }

    /**
     *
     */
    public function import()
    {
        set_time_limit(0);

        if (!ini_get("auto_detect_line_endings")) {
            ini_set("auto_detect_line_endings", '1');
        }

        $file = Input::file('providers_file');

        $csv = Reader::createFromFileObject($file->openFile());

        $keys = ['first_name', 'last_name', 'practice_name', 'feeplan', 'address1', 'address2',
            'city', 'state', 'zipcode', 'phone', 'fax', 'email', 'website', 'specialty',
            'tax_id'
        ];


        $results = $csv->fetchAssoc($keys);

        $specialtyIdByName = [];
        $scheduleIdByName = [];

        $headerRead = 0;

        foreach ($results as $row) {
            if (!$headerRead++) {
                continue;
            }

            // check specialty
            if (!empty($row['specialty']) && !isset($specialtyIdByName[$row['specialty']])) {
                $specialty = Specialty::where('name', $row['specialty'])->first();

                if (!$specialty) {
                    $specialty = Specialty::create([
                        'name' => $row['specialty']
                    ]);
                }

                $specialtyIdByName[$row['specialty']] = $specialty->id;
            }

            // check fee plan
            if (!empty($row['feeplan']) && !isset($scheduleIdByName[$row['feeplan']])) {
                $schedule = Schedule::where('name', $row['feeplan'])->first();

                if (!$schedule) {
                    $schedule = Schedule::create([
                        'name' => $row['feeplan']
                    ]);
                }

                $scheduleIdByName[$row['feeplan']] = $schedule->id;
            }

            // check if provider already exists
            $provider = Provider::where('first_name', $row['first_name'])
                ->where('last_name', $row['last_name'])
                ->where('practice_name', $row['practice_name'])
                ->where('address1', $row['address1'])
                ->where('city', $row['city'])
                ->where('state', $row['state'])
                ->where('zipcode', $row['zipcode'])->first();

            // if provider exists only update specialties
            if ($provider) {
                try {
                    $provider->specialties()->attach($specialtyIdByName[$row['specialty']]);
                } catch (\Exception $e) {
                    // probably a duplicated record
                }
            } else {
                // create provider
                $provider = new Provider();
                $provider->first_name = $row['first_name'];
                $provider->last_name = $row['last_name'];
                $provider->practice_name = $row['practice_name'];
                $provider->address1 = $row['address1'];
                $provider->address2 = $row['address2'];
                $provider->city = $row['city'];
                $provider->state = $row['state'];
                $provider->zipcode = $row['zipcode'];
                $provider->phone1 = $row['phone'];
                $provider->fax = $row['fax'];
                $provider->email = $row['email'];
                $provider->website = $row['website'];
                $provider->tax_id = $row['tax_id'];

                if (isset($scheduleIdByName[$row['feeplan']])) {
                    $provider->schedule_id = $scheduleIdByName[$row['feeplan']];
                }

                try {
                    $provider->save();
                } catch (\Exception $e) {
                    Log::info('IMPORTING ERROR: ' . $e->getMessage());
                }

                if (isset($specialtyIdByName[$row['specialty']])) {
                    $provider->specialties()->attach($specialtyIdByName[$row['specialty']]);
                }
            }
        }
    }

    public function export($type = 'full')
    {
        set_time_limit(0);

        $pdo = DB::connection()->getPdo();

        if ($type == 'dpi') {
            $fields = [
                "practice_name",
                "first_name",
                "last_name",
                "specialty",
                "address1",
                "address2",
                "city",
                "state",
                "zipcode",
                "phone1"
            ];

            $sql = "SELECT 
                        p.practice_name,
                        p.first_name,
                        p.last_name,
                        sp.name as specialty,
                        p.address1,
                        p.address2,
                        p.city,
                        p.state,
                        p.zipcode,
                        p.phone1                          
                    FROM providers p 
                    LEFT JOIN provider_specialty ps ON ps.provider_id = p.id 
                    LEFT JOIN specialties sp ON ps.specialty_id = sp.id";

        } else {

            $fields = [
                "practice_name",
                "first_name",
                "last_name",
                "address1",
                "address2",
                "city",
                "state",
                "zipcode",
                "phone1",
                "phone2",
                "fax",
                "email",
                "website",
                "tax_id",
                "location_office_manager_name",
                "location_office_manager_phone",
                "location_office_manager_email",
                "created_at",
                "specialty",
                "status",
                "fee_schedule",
                "provider_type"
            ];

            $sql = "SELECT
                        p.practice_name,
                        p.first_name,
                        p.last_name,
                        p.address1,
                        p.address2,
                        p.city,
                        p.state,
                        p.zipcode,
                        p.phone1,
                        p.phone2,
                        p.fax,
                        p.email,
                        p.website,
                        p.tax_id,
                        p.location_office_manager_name,
                        p.location_office_manager_phone,
                        p.location_office_manager_email,
                        p.created_at,
                        sp.name as specialty, 
                        ss.name as status, 
                        sc.name as fee_schedule, 
                        pt.name as provider_type 
                    FROM providers p 
                    LEFT JOIN provider_specialty ps ON ps.provider_id = p.id 
                    LEFT JOIN specialties sp ON ps.specialty_id = sp.id
                    LEFT JOIN statuses ss ON ss.id = p.status_id
                    LEFT JOIN schedules sc ON sc.id = p.schedule_id
                    LEFT JOIN provider_types pt ON pt.id = p.provider_type_id";
        }

        $results = $pdo->query($sql, \PDO::FETCH_ASSOC);

        $writer = Writer::createFromFileObject(new \SplTempFileObject());
        $writer->insertOne($fields);
        $writer->insertAll($results);
        $writer->output('providers_' . date('d_m_Y') . "_$type.csv");

    }
}

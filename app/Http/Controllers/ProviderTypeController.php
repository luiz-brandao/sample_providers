<?php

namespace App\Http\Controllers;

use App\ProviderType;
use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Facades\Datatables;

class ProviderTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Datatables::of(ProviderType::query())
            ->setRowId('id')
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $provider_type = ProviderType::create(Input::get());

        return response()->json(['resource' => $provider_type]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provider_type = ProviderType::findOrFail($id);

        return response()->json(['resource' => $provider_type]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $provider_type = ProviderType::findOrFail($id)->update(Input::get());

        return response()->json(['provider_type' => $provider_type]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProviderType::destroy($id);

        return response()->json(['status' => true]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Schedule;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;


class ScheduleController extends Controller
{
    protected $fileDestinationPath;

    public function __construct()
    {
        $this->fileDestinationPath = public_path('schedule_files');

        ini_set('post_max_size', '10M');
        ini_set('upload_max_filesize', '10M');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = Schedule::select(['id','name','filepath', 'filename']);

        return Datatables::of($schedules)
            ->addColumn('filepath', function($schedule){
                return "<a target='new' href='/schedule_files/$schedule->filename'><i class='file pdf outline icon'></i> $schedule->filename</a>";
            })
            ->setRowId('id')
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileInfo = $this->processFileUpload($request);

        $schedule = Schedule::create([
            'name' => Input::get('name'),
            'filename' => $fileInfo['name'],
            'filepath' => $fileInfo['path'],
        ]);

        return response()->json(['resource' => $schedule]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedule = Schedule::findOrFail($id);

        return response()->json(['resource' => $schedule]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hasFile('file')) {
            $fileInfo = $this->processFileUpload($request);
            $data = [
                'name' => Input::get('name'),
                'filename' => $fileInfo['name'],
                'filepath' => $fileInfo['path'],
            ];
        } else {
            $data = [
                'name' => Input::get('name'),
            ];
        }

        $schedule = Schedule::findOrFail($id)->update($data);

        return response()->json(['resource' => $schedule]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Schedule::destroy($id);

        return response()->json(['status' => true]);
    }

    private function processFileUpload($request){
        if ($request->hasFile('file')){
            $file = $request->file('file');

            if($file->isValid()){
                $originalName = $file->getClientOriginalName();
                $filepath = $file->move($this->fileDestinationPath, $originalName);
            } else {
                abort(500, 'Invalid file.');
            }
        } else {
            $originalName = $filepath = '';
        }

        return ['name' => $originalName, 'path' => $filepath];
    }
}

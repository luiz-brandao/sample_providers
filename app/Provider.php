<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Provider
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $zipcode
 * @property string $phone1
 * @property string $phone2
 * @property string $fax
 * @property string $email
 * @property string $website
 * @property string $practice_name
 * @property string $tax_id
 * @property string $location_office_manager_name
 * @property string $location_office_manager_phone
 * @property string $location_office_manager_email
 * @property string $agreement_files
 * @property string $active_date
 * @property integer $specialty_id
 * @property integer $provider_type_id
 * @property integer $schedule_id
 * @property integer $status_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Specialty $specialty
 * @property-read \App\ProviderType $type
 * @property-read \App\Schedule $schedule
 * @property-read \App\Status $status
 * @property-read \App\Agreement $agreement
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereAddress1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereAddress2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereZipcode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider wherePhone1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider wherePhone2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereFax($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereWebsite($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider wherePracticeName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereTaxId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereLocationOfficeManagerName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereLocationOfficeManagerPhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereLocationOfficeManagerEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereAgreementFiles($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereActiveDate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereSpecialtyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereProviderTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereScheduleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Provider whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Provider extends Model
{
    protected $guarded = ['id'];

    public function setScheduleIdAttribute($id)
    {
        $this->attributes['schedule_id'] = $this->getIdValueAfterNullCheck($id);
    }

    public function setStatusIdAttribute($id)
    {
        $this->attributes['status_id'] = $this->getIdValueAfterNullCheck($id);
    }

    protected function getIdValueAfterNullCheck($id){
        $id = trim($id);
        if($id == '' || $id == 'null'){
            $id = null;
        }
        return $id;
    }

    
    public function specialties()
    {
        return $this->belongsToMany('App\Specialty')->select(['id', 'name']);
    }

    public function type()
    {
        return $this->belongsTo('App\ProviderType');
    }

    public function schedule()
    {
        return $this->belongsTo('App\Schedule');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function agreements()
    {
        return $this->hasMany('App\Agreement');
    }
    
    

}

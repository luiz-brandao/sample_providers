var ajaxUrl = "/user";

var crudFormConfig = {
    fields: {
        'name': 'empty',
        'email': 'empty',
        //'password': 'empty'
    }
};

var dataTablesColumns = [
    {data: 'name', name: 'name'},
    {data: 'email', name: 'email'},
    {data: 'is_admin', name: 'is_admin'},
];

function getSchema() {
    return {
        id: '',
        name: '',
        email: '',
        password: '',
        is_admin: ''
    }
}

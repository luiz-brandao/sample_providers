var ajaxUrl = "/schedule";

var crudFormConfig = {
    fields: {
        'name': 'empty'
    }
};

var dataTablesColumns = [
    {data: 'id', name: 'id', width:50 },
    {data: 'name', name: 'name'},
    {data: 'filepath', name: 'filepath'},
];

function getSchema() {
    return {
        id: '',
        name: '',
        filepath: ''
    }
}

var mixins = [{
    methods: {
        saveAndUploadSchedule: function () {
            if (!$crudForm.form("is valid")) {
                return;
            }

            var formData = new FormData();
            formData.append("name", this.fields.name);
            formData.append("file", $("#file")[0].files[0]);

            switch (this.crudActionType) {
                case 'Update':
                    var url = ajaxUrl + '/' + this.fields.id;
                    formData.append('_method', 'PATCH');
                    break;
                default:
                    var url = ajaxUrl;
            }

            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    $dataTable.ajax.reload();
                    vm.closeCrudModal();
                },
                error: function(jqXHR, textStatus, errorMessage) {
                    vm.crudError = "There was an error";
                }
            });

        }
    }
}];
var ajaxUrl = "/specialty";
var crudFormConfig = {
    fields: {
        'name': 'empty'
    }
};
function getSchema() {
    return {
        id: '',
        name: ''
    }
}


var vm;
var $crudForm;
var $crudModal;
var $dataTable;

$(function () {
    vm = new Vue({
        el: 'body',
        data: {
            ajaxUrl: "/specialty",
            crudActionType: '',
            crudError: null,
            fields: {}
        },
        ready: function () {
            this.resetFields();
        },
        methods: {
            getSchema: function () {
                return getSchema();
               /* return {
                    id: '',
                    name: ''
                }*/
            },
            resetFields: function () {
                this.fields = this.getSchema();
                this.crudError = null;
            },
            save: function () {
                if ($crudForm.form("is valid")) {

                    var promise;
                    switch (this.crudActionType) {
                        case 'Update':
                            promise = this.$http.patch(this.ajaxUrl + '/' + this.fields.id, this.fields);
                            break;
                        default:
                            promise = this.$http.post(this.ajaxUrl, this.fields);
                    }

                    promise.then(function (response) {
                        $dataTable.ajax.reload();
                        this.closeCrudModal();
                    }, function (response) {
                        this.crudError = "There was an error";
                    });
                }
            },
            delete: function () {
                this.$http.delete(this.ajaxUrl + '/' + this.fields.id).then(function (response) {
                    $dataTable.ajax.reload();
                    this.closeCrudModal();
                }, function (response) {
                    this.crudError = "There was an error";
                });
            },
            showCrudDelete: function () {
                $('#confirmDeleteModal').modal({
                    onApprove: function () {
                        vm.delete();
                    }
                }).modal('show');
            },
            showCrudModal: function (action) {
                this.crudActionType = action;
                $crudModal.modal("show");
            },
            showCreateModal: function () {
                vm.resetFields();
                this.showCrudModal('Create');
            },
            showUpdateModal: function (id) {
                this.$http.get(this.ajaxUrl + '/' + id).then(function (response) {
                    this.fields = response.data.specialty;
                    this.showCrudModal('Update');
                }, function (response) {
                    this.crudError = "There was an error";
                });
            },
            closeCrudModal: function () {
                $crudModal.modal("hide");
                this.resetFields();
            },
        }
    });

    // form config
    $crudForm = $('#crudForm').form({
        fields: {
            'name': 'empty'
        }
    });

    // modal config
    $crudModal = $("#crudModal").modal();

    // datatable config
    $dataTable = $('#crudTable').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": vm.ajaxUrl,
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
        ]
    });

    $('#crudTable tbody').on('click', 'tr', function () {
        vm.showUpdateModal($(this)[0].id);
    });

});
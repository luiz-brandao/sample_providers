var ajaxUrl = "/provider";

var crudFormConfig = {
    fields: {
        'first_name': 'empty',
        'last_name': 'empty',
        'address1': 'empty',
        'city': 'empty',
        'state': 'empty',
        'zipcode': 'empty',
        'phone1': 'empty',
        'practice_name': 'empty',
        'specialties': 'empty',
        'provider_type_id': 'empty'
    }
};

var dataTablesColumns = [
    {data: 'id', name: 'id', width: 50,  "visible": false},
    {data: 'first_name', name: 'first_name'},
    {data: 'last_name', name: 'last_name'},
    {data: 'practice_name', name: 'practice_name'},
    {data: 'city', name: 'city'},
    {data: 'state', name: 'state'},
    {data: 'zipcode', name: 'zipcode'},
];

function getSchema() {
    return {
        id: '',
        first_name: '',
        last_name: '',
        address1: '',
        address2: '',
        city: '',
        state: '',
        zipcode: '',
        phone1: '',
        phone2: '',
        fax: '',
        email: '',
        website: '',
        practice_name: '',
        tax_id: '',
        location_office_manager_name: '',
        location_office_manager_phone: '',
        location_office_manager_email: '',
        //agreement_files: '',
        active_date: '',
        specialties: [],
        //specialty_id: '',
        provider_type_id: '',
        schedule_id: '',
        status_id: ''
    }
}

var mixins = [{
    data: {
        options: {},
    },
    created: function () {
        this.$http.get(ajaxUrl + '/options').then(function (response) {
            this.options = response.data;
        });
    },
    watch: {
        'modalOpen': function (val, oldVal) {
            // we do this here because otherwise the animation of the modal gets affected
            $.each(this.fields.specialties, function (index, value) {
                $('#specialties-select').dropdown("set selected", value.id);
            });
        }
    },
    methods: {
        importFile: function () {
            var formData = new FormData();
            formData.append("providers_file", $("#import-upload")[0].files[0]);

            $("#import-upload-btn").addClass('loading');

            $.ajax({
                url:  ajaxUrl + '/import',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {
                    $dataTable.ajax.reload();
                    vm.closeCrudModal();
                 },
                error: function (jqXHR, textStatus, errorMessage) {
                    vm.crudError = "There was an error";
                },
                complete: function(response){
                    $("#import-upload-btn").removeClass('loading');
                    $("#import-upload").val("");
                }
            });
        },
        addFile: function () {
            this.totalNewFiles++;

            Vue.nextTick(function () {
                $(".agreement-file").last().click();
            });
        },
        deleteAgreement: function (id) {
            this.$http.delete(ajaxUrl + '/agreement/' + id).then(function (response) {
                if (response.data.status) {
                    for (var i in this.fields.agreements) {
                        if (this.fields.agreements[i].id == id) {
                            this.fields.agreements.$remove(this.fields.agreements[i])
                        }
                    }

                }
            });
        },
        saveAndUploadProvider: function () {
            if (!$crudForm.form("is valid")) {
                return;
            }

            var formData = new FormData();
            for (var key in this.fields) {
                formData.append(key, this.fields[key])
            }

            $(".agreement-file").each(function (index, file) {
                if (file.files[0]) {
                    formData.append("agreement_files[]", file.files[0]);
                }
            });

            // we use it to get agreements information, but we don't want to send it back to save
            formData.delete("agreements");

            switch (this.crudActionType) {
                case 'Update':
                    var url = ajaxUrl + '/' + this.fields.id;
                    formData.append('_method', 'PATCH');
                    break;
                default:
                    var url = ajaxUrl;
            }

            $.ajax({
                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (response) {
                    $dataTable.ajax.reload();
                    vm.closeCrudModal();
                    //vm.totalNewFiles = 0;
                },
                error: function (jqXHR, textStatus, errorMessage) {
                    vm.crudError = "There was an error";
                }
            });

        }
    }
}];

$(function () {
    $('#specialties-select').dropdown({
        'onChange': function (value, text, choice) {
            vm.fields.specialties = value;
        }
    });
});
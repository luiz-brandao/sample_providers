var vm;
var $crudForm;
var $crudModal;
var $dataTable;

$(function () {
    vm = new Vue({
        el: 'body',
        mixins: (typeof mixins !== 'undefined') ? mixins : [],
        data: {
            ajaxUrl: ajaxUrl,
            crudActionType: '',
            crudError: null,
            totalNewFiles: null,
            fields: {},
            modalOpen: false
        },
        ready: function () {
            this.resetFields();
        },
        methods: {
            getSchema: function () {
                return getSchema();
            },
            resetFields: function () {
                $(".ui.dropdown").dropdown('clear');
                this.fields = this.getSchema();
                this.crudError = null;
                this.totalNewFiles = 0;
            },
            save: function () {
                if (!$crudForm.form("is valid")) {
                    return;
                }

                var promise;
                switch (this.crudActionType) {
                    case 'Update':
                        promise = this.$http.patch(this.ajaxUrl + '/' + this.fields.id, this.fields);
                        break;
                    default:
                        promise = this.$http.post(this.ajaxUrl, this.fields);
                }

                promise.then(function (response) {
                    $dataTable.ajax.reload();
                    this.closeCrudModal();
                }, function (response) {
                    this.crudError = "There was an error";
                });
                
            },
            deleteResource: function () {
                this.$http.delete(this.ajaxUrl + '/' + this.fields.id).then(function (response) {
                    $dataTable.ajax.reload();
                    this.closeCrudModal();
                }, function (response) {
                    this.crudError = "There was an error";
                });
            },
            showCrudDelete: function () {
                $('#confirmDeleteModal').modal({
                    onApprove: function () {
                        vm.deleteResource();
                    }
                }).modal('show');
            },
            showCrudModal: function (action) {
                this.crudActionType = action;
                $crudModal.modal("show");
            },
            showCreateModal: function () {
                vm.resetFields();
                this.showCrudModal('Create');
            },
            showUpdateModal: function (id) {
                this.resetFields();
                this.$http.get(this.ajaxUrl + '/' + id).then(function (response) {
                    this.fields = response.data.resource;
                    this.showCrudModal('Update');
                }, function (response) {
                    this.crudError = "There was an error";
                });
            },
            closeCrudModal: function () {
                $crudModal.modal("hide");
                this.resetFields();
            },
        }
    });

    // form config
    $crudForm = $('#crudForm').form(crudFormConfig);

    // modal config
    $crudModal = $("#crudModal").modal({
        onVisible: function(){
            vm.modalOpen = true;
        },
        onHidden: function(){
            vm.modalOpen = false;
        },
    });

    // datatable config
    $dataTable = $('#crudTable').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": vm.ajaxUrl,
        columns: dataTablesColumns,
        "lengthMenu": [ 10, 20, 50, 75, 100 ],
        "pageLength": 20
    });

    $('#crudTable tbody').on('click', 'tr', function () {
        vm.showUpdateModal($(this)[0].id);
    });

});